from random import randint

# Asks for name
name = input("Hi! What is your name? ")

for guess_number in range(1, 6):
    # Generate a random month number
    month = randint(1, 12)
    # Generate a random year number
    year = randint(1924, 2004)

    # print the formatted message
    print(f"Guess {guess_number}: {name} were you born in {month}/{year}?")
    # then prompt with yes or no
    response = input("yes or no? ")

    # if response is "yes"
    if response == "yes":
        # then print out the message
        print("I knew it!")
        # exit when the response is "yes"
        exit()
    elif guess_number < 5:
        # otherwise print out another message
        print("Drat! Lemme try again!")
    else:
        print("I have other things to do. Good bye.")
